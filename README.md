# Frontend Mentor - QR code component

[challenge:QR code component](https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H)

# Resultados


## 👉visualizacion en una laptop
<br><br>


<img src="laptop.png" >
<br><br>


## 👉visualizacion en una mobile
<br><br>

<img src="mobile.png">


<br><br>

# Desplegado 👉 [click](https://lucent-basbousa-093fee.netlify.app/)

